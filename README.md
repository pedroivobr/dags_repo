# Repositório de DAGs do AirFlow

<a href="https://gitmoji.dev">
  <img src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square" alt="Gitmoji">
</a>

## Sobre

O projeto do repositórico das DAGs do AirFlow é uma iniciativa para a automatização do processo de desenvolvimento das dags dentro do AirFlow.

Dessa maneira o projeto consiste em criar o espaço para o desenvolvimento de DAGs em conjunto, a automatização dos testes e o deploy do projeto ( ainda em desenvolvimento😔).

Esse projeto é relacionado com o [AirFlow](https://gitlab.com/pedroivobr/airflow), que é um docker-compose para subir um ambiente pronto do airflow e esse projeto serve para o desenvolvimento das DAGs dele.

## Usando

Para usar se deve ter uma instância do AirFlow rodando na máquina, e a pasta das DAGs apontando para esse projeto do git.

Por exemplo, no DockerCompose do AirFlow coloca nos volumes como descrito acima:

```
volumes:
    - ../dags_repo:/opt/airflow/dags
```

Assim as DAGs seram reconhecidas, e para adicionar novas basta adicionar o `arquivo_dag.py` nesse repositório.
