from datetime import timedelta
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

default_args = {
    'owner' : 'airflow',
    'start_date' : days_ago(1),
    'depends_on_past' : False,
    'retries' : 1,
    'retry_delay' : timedelta(minutes=5)
}

dag = DAG('dw_stg_gsheets',
    default_args=default_args,
    catchup = False,
    description='Utilizando a biblioteca que facilita a comunicação com o googlesheets.',
    schedule_interval='15 8 * * *')

def stg_gsheets():
    import pandas as pd
    from bibliotecas.SheetsAPI import SheetsAPI

    # If modifying these scopes, delete the file token.json.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

    # The ID and range of a sample spreadsheet.
    SAMPLE_SPREADSHEET_ID = '1GxS9zXtizDUOCn7s5tcuYzYSH5w71nvMEw6appahDNM'

    # instanciando objeto da api
    planilha = SheetsAPI(SAMPLE_SPREADSHEET_ID, SCOPES)

    # pegando todos os sheet_names da planilha
    lista_snames = planilha.sheet_names()

    # baixar as duas primeiras sheets
    aux = planilha.download_lista(lista_snames[:2])

    # printar as sheets baixadas
    print(pd.concat(aux))

t = PythonOperator(task_id='download_gsheets',
python_callable=stg_gsheets,
dag=dag
)

## sequência das tasks
t