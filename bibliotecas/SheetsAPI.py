from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import pandas as pd
from airflow.models import Variable

class SheetsAPI:

    def __init__(self, sheet_id, scopes):
        self.sheet_id = sheet_id
        self.scopes = scopes
        self.token_path = Variable.get("token_google_api")
        self.credential_path = Variable.get("credential_google_api")

        ## credenciais
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(self.token_path):
            creds = Credentials.from_authorized_user_file(self.token_path, self.scopes)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.credential_path, self.scopes)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(self.token_path, 'w') as token:
                token.write(creds.to_json())

        self.creds = creds

    def sheet_names(self):
        service = build('sheets', 'v4', credentials=self.creds)
        # Call the Sheets API
        sheet = service.spreadsheets()
        result = sheet.get(spreadsheetId=self.sheet_id).execute()
        values = result.get('sheets', [])
        sheet_names = []
        for name in values:
            sheet_names.append(name['properties']['title'])
        return sheet_names
    
    def download(self, sheet_name):
        service = build('sheets', 'v4', credentials=self.creds)
        # Call the Sheets API
        sheet = service.spreadsheets()
        result = sheet.values().get(spreadsheetId=self.sheet_id,
                                    range=sheet_name).execute()
        values = result.get('values', [])

        df_download = pd.DataFrame(values[1:], columns=values[0])

        return df_download
    
    def download_lista(self, sheet_name):
        service = build('sheets', 'v4', credentials=self.creds)
        # Call the Sheets API
        sheet = service.spreadsheets()

        list_df = []

        for sn in sheet_name:
            print("vez do:",sn)
            result = sheet.values().get(spreadsheetId=self.sheet_id,range=sn).execute()
            
            values = result.get('values', [])

            df_download = pd.DataFrame(values[1:], columns=values[0])

            list_df.append(df_download)

            del df_download

        return list_df    